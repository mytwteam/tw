<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\SchoolClass;
use App\Models\Student;
use App\Models\User;
use Validator;
use Response;

class StudentController extends Controller
{
   
    /**
     * list class students 
     *
     * @return \Illuminate\Http\Response
     */
    public function listStudents(Request $request)
    {
        if(!$this->checkUserLogin($request))//check user login
        {
            return Response::json([
                'result'  => [],
                'error' => 'InvalidTokent',
                'status'   => 'ERROR'
            ],401);
        }

        //validate required fields
        $validator = Validator::make($request->all(), [
            'classeId'        => 'required',
        ]);
        if ($validator->fails()) {
            return Response::json([
                'result'  => [],
                'error'   => 'RequiredFieldsNull',
                'status'  => 'ERROR'
            ],422);
        }
        //take class id related students details
        $classe = $request->classeId;
        $data['students'] = Student::join('school_classes','school_classes.classe_id','=','students.classe_id')
                                    ->join('grades','grades.grade_id','=','school_classes.grade_id')
                                    ->where('students.classe_id',$classe)
                                    ->select('students.*','grade','classe_name')
                                    ->get();
        return Response::json([
            'result'  => $data,
            'error'   => '',
            'status'  => 'SUCCESS'
        ],200);
    }

    /**
     * save student details
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        //check user login
        if(!$this->checkUserLogin($request))
        {
            return Response::json([
                'result'  => [],
                'error' => 'InvalidTokent',
                'status'   => 'ERROR'
            ],401);
        }
        //validate required fields
        $validator = Validator::make($request->all(), [
            'classeId'       => 'required',
            'studentName'    => 'required',
            'studentAddress' => 'required',
        ]);
        //validation false
        if ($validator->fails()) 
        {
            return Response::json([
                'result'  => [],
                'error'   => 'RequiredFieldsNull',
                'status'  => 'ERROR'
            ],422);
        }
        
        //insert new student details
        $student                  = new Student;
        $student->classe_id       = $request->classeId;
        $student->student_name    = $request->studentName;
        $student->student_address = $request->studentAddress;
        $student->save();
        
        return Response::json([
            'result'  => '',
            'error' => '',
            'status'   => 'SUCCESS'
        ],200);
        
    }

    /*
    *check user login
    */
    private function checkUserLogin(Request $request)
    {
        //token validate
        if(isset($request->token)&& ($request->token!=''))
        {
            $user = User::where('token',$request->token)->first();
            if(count($user)>0)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    
    
}

