<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Hash;
use Auth;
use Response;

class UserController extends Controller
{
    /**
     * login user
     * @param Request $request
     * @return user token
    */
    public function userLogin(Request $request)
    {
        $email     = $request->email;
        $password  = $request->password;
        
        if (Auth::attempt(['email' => $email, 'password' => $password])) 
        {
            $user=Auth::user();
            //create a token and insert send the tocken to app
            $newToken =  str_random(32);
            
            // set first time use to 0
            $user->token = $newToken;
            $user->save();
            
            return Response::json([
                'result'  => $user,
                'error' => '',
                'status'   => 'SUCCESS'
            ],200);
        }
        else
        {
            return Response::json([
                'result'  => [],
                'error' => 'InvalidLogin',
                'status'   => 'ERROR'
            ],402);
        }
    }
    
    /**
     * logout user 
     * @param Request $request
     * @return remove user token 
    */
    public function userLogout(Request $request) 
    {
        User::where('token',$request->token)->update(['token'=>null]);
        return Response::json([
                'result'  => '',
                'error' => '',
                'status'   => 'SUCCESS'
        ],200);
    }
    
    
}
