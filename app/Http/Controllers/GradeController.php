<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Grade;


class GradeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show add grades page
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        return view('grades.add');
    }

    /**
     * list added grades 
     *
     * @return \Illuminate\Http\Response
     */
    public function lisGrades(Request $request)
    {
        $data['grades'] = Grade::all();
        return view('grades.list',$data);
    }

    /**
     * save grades details
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        
        $v = $this->validate($request, [
            'gradeName'    => 'required',
        ]);
        
        $grade            = new Grade;
        $grade->grade     = $request->input('gradeName');
        $grade->save();
        return redirect('grades/list');
        
    }
    
    /**
     * delete reward details
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteGrades($id,Request $request)
    {
        Reward::find($id)->delete();
        return redirect('grades/list');
    }
    
    
}

