<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\SchoolClass;


class SchoolClasseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth');
     }


    /**
     * Show add grades page
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        
        //take Grades
        $data['grades']   = Grade::select('*')->get();
        return view('school_classes.add_classes',$data);
    }

    /**
     * list added grades 
     *
     * @return \Illuminate\Http\Response
     */
    public function listClasses($id,Request $request)
    {
        $data['classes'] = SchoolClass::where('grade_id',$id)->get();
        return view('school_classes.list',$data);
    }

    /**
     * list added grades 
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(Request $request)
    {
        $result = [];
        //take Grades
        $grades = Grade::get();
        $i=0;
        //Grades exist
        if(count($grades)>0)
        {
            foreach ($grades as $grade) 
            {
                $gradeId = $grade->grade_id;
                $result[$i]['grade'] = $grade->grade;
                $result[$i]['gradeId'] = $grade->grade_id;
                //take eac grade realted classes
                $classes = SchoolClass::where('grade_id',$gradeId)->get();
                $result[$i]['classes'] = $classes;
                $i++;
            }
        }
        $data['details'] = $result;
        return view('school_classes.list_all',$data);
    }

    /**
     * save grades details
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        
        $v = $this->validate($request, [
            'className'    => 'required',
            'gradeId'      => 'required',
        ]);
        $gradeId            = $request->input('gradeId');
        $grade              = new SchoolClass;
        $grade->classe_name = $request->input('className');
        $grade->grade_id    = $request->input('gradeId');
        $grade->save();
        return redirect('classes/list/'.$gradeId);
        
    }
    
    /**
     * delete classes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteClasses($id,Request $request)
    {
        SchoolClass::find($id)->delete();
        return redirect('school_classes/list');
    }
    
    
}

