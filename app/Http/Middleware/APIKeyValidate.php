<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Response;
use Request;
use App\Models\User; 

class APIKeyValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->token!='')
        {
            $user = User::where('token',$token)->first();
            if(count($user)>0)
            {
                return $next($request);
            }
            else 
            {
                return Response::json([
                'result'  => [],
                'error' => 'InvalidTokent',
                'status'   => 'ERROR'
                ],401);
            }
        }
        else
        {
            return Response::json([
                'result'  => [],
                'error' => 'InvalidTokent',
                'status'   => 'ERROR'
            ],401);
        }
    }
}
