<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::post('password/reset', 'API\UserController@postReset');



Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/grades/add',['uses'=>'GradeController@add']);
    Route::post('/grades/save',['uses'=>'GradeController@save']);
    Route::get('/grades/list',['uses'=>'GradeController@lisGrades']);

    Route::get('/classes/add',['uses'=>'SchoolClasseController@add']);
    Route::post('/classes/save',['uses'=>'SchoolClasseController@save']);
    Route::get('/classes/list/{id}',['uses'=>'SchoolClasseController@listClasses']);
    Route::get('/list',['uses'=>'SchoolClasseController@listAll']);

    Route::get('/class/{id}/students/list/',['uses'=>'StudentController@listStudents']);
    Route::get('/students/all/list',['uses'=>'StudentController@listAllStudents']);
    Route::get('/students/add/{id}',['uses'=>'StudentController@add']);
    Route::post('/students/save',['uses'=>'StudentController@save']);

    Route::get('/settings',['uses'=>'SettingController@index']);
    Route::get('/settings/email/update',['uses'=>'SettingController@getEmailUpdate']);
    Route::get('/settings/password/update',['uses'=>'SettingController@getPasswordUpdate']);
    Route::post('/settings/email/update',['uses'=>'SettingController@postEmailUpdate']);
    Route::post('/settings/password/update',['uses'=>'SettingController@postPasswordUpdate']);
});

// API calls with no access token guarded
Route::group(['prefix' => 'api/'], function () {
    Route::post('/user/login',['uses'=>'API\UserController@userLogin']);
    
    // API calls with  access token guarded
    Route::post('/user/logout',['uses'=>'API\UserController@userLogout']);
    Route::post('/classe/students/list',['uses'=>'API\StudentController@listStudents']);
    Route::post('/classe/students/add',['uses'=>'API\StudentController@save']);

    Route::post('/school/list',['uses'=>'API\SchoolClasseController@listAll']);
    Route::post('/classe/delete',['uses'=>'API\SchoolClasseController@deleteClasses']);
});