<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    protected $table       = 'school_classes';
    protected $primaryKey  = 'classe_id';
    protected $fillable    = ['classe_name','grade_id'];
    protected $hidden      = ['created_at','updated_at'];
    
}
