@extends('layouts.app')

@section('content')
<link href="{{URL('css/prettyPhoto.css')}}" rel="stylesheet">
<script src="{{URL('js/jquery.prettyPhoto.js')}}"></script>
<div class="panel panel-default">
    <div class="col-sm-12">
        <h4 id="overview" class="page-header"><a href="#"><strong> {{  strtoupper('grades')}}</strong></a></h4>
    </div>
    
    <div class="panel-body">
        <div class="row"><a href="{{URL('grades/add')}}" class="btn btn-primary pull-right">Add new grade</a></div>
        <br/>
        <br/>
        @if(count($grades)>0)
        <table class="table  table-bordered app_user_table">
            <thead>
                <tr>
                    <th >Grade</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($grades as $grade)
                <tr>
                    <td >{{$grade->grade}}</td>
                    <td>
                        <a href="{{URL('classes/list/'.$grade->grade_id)}}" >view classes</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        No records found
        
        @endif
        
    </div>
</div>
<script>
$(document).ready(function(){
 $('.app_user_table').DataTable();
});

//block/unblock confirmation
function delete_confirmation()
{
    var result = confirm("Do you want to delete this reward?");
    if(result)
    {
        return true;
    }
    else
    {
        return false;
    }
}
</script>    
@endsection
