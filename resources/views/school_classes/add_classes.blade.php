@extends('layouts.app')
@section('content')
<div class="panel panel-default">
    <div class="col-sm-12">
        <h4 id="overview" class="page-header"><a href="#"><strong> {{  strtoupper('classes')}}</strong></a></h4>
    </div>
   
                    <?php for($i=0; $i<count($grades);$i++){ ?>
                            <p value="{{$grades[$i]->grade_id}}">{{$grades[$i]->grade_name}}</p>
                    <?php } ?>
                   
    <div class="panel-body">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
        @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
        @endforeach
                </ul>
            </div>
        @endif
        
        <form class="form-horizontal" action="{{URL('classes/save')}}" method="POST" files="true" enctype="multipart/form-data">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Grade Name</label>
                <div class="col-sm-6">
                    @if($grades)
                    <select name="gradeId">
                        @foreach($grades as $grade)
                        <option value="{{$grade->grade_id}}">{{$grade->grade}}</option>
                        @endforeach
                    </select>
                    @endif
                </div>
            </div><div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Class Name</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="className" name="className" value="{{old('className')}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
