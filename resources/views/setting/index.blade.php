@extends('layouts.app')

@section('content')
<style>
    .dimension_table th{
        text-align: center;
    }
</style>
<div class="panel panel-default">
    <div class="col-sm-12">
        <h4 id="overview" class="page-header"><a href="#"><strong> {{  strtoupper('Settings')}}</strong></a></h4>
    </div>
    
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3 col-lg-offset-4">
                <a href="{{url('settings/email/update')}}" class="btn btn-default col-lg-10" role="button"> Update Email </a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-3 col-lg-offset-4">
                <a href="{{url('settings/password/update')}}" class="btn btn-default col-lg-10" role="button"> Update Password </a>
            </div>
        </div>
       
    </div>
    
</div>

@endsection
