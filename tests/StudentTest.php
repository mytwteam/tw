<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentTest extends TestCase
{
    /**
     * validate student listing
     *
     * @return void
     */
    public function testStudentListingTrue()
    {
        $this->json('POST', '/students/list', ['classeId' => 2,'token'=>'IgpPEqN1C1dLC8bSCE14esA3fPywvqc3'])
             ->seeJson([
                 'created' => true,
             ]);
    }

    /**
     * validate student listing
     *
     * @return void
     */
    public function testSchoolListingTrue()
    {
        $this->json('POST', '/school/list', ['token'=>'IgpPEqN1C1dLC8bSCE14esA3fPywvqc3'])
             ->seeJson([
                 'created' => true,
             ]);
    }
}
